namespace Lebrony
{
    internal class Branch
    {
        public List<Branch> Branches = new List<Branch>();
        public Branch Parent {get; set}
        Account account;
        Loan loan;

        public Branch(Account account, Loan loan)
        {
            this.account = account;
            this.loan = loan;
        }
    }
}
