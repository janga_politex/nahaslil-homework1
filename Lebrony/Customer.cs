﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lebrony
{
    internal class Customer
    {
        public Saving_Account saving_Account { get; set; }
        public Current_Account current_Account { get; set; }
        public Loan loan  { get; set; }
    }
}
